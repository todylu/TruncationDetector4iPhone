//
//  WindowSniffer.h
//  TruncationDetector
//
//  Created by taodongl on 8/18/15.
//  Copyright (c) 2015 taodongl. All rights reserved.
//

#ifndef TruncationDetector_WindowSniffer_h
#define TruncationDetector_WindowSniffer_h
@interface WindowSniffer : NSObject
{
    UIWindow * mainWindow;
}
@property (nonatomic, strong, readwrite) NSMutableDictionary * snifferData;
- (NSString *) reportHtml;
- (NSString *) reportJson;
@end

#endif
