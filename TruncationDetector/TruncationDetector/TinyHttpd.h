//
//  TinyHttpd.h
//  TruncationDetector
//
//  Created by Taodong Lu on 8/16/15.
//  Copyright (c) 2015 Taodong Lu. All rights reserved.
//

#ifndef TruncationDetector_TinyHttpd_h
#define TruncationDetector_TinyHttpd_h
@interface TinyHttpd : NSObject
{
    NSString* method;
    NSString* uri;
    NSMutableArray* headers;
}

- (id) initWithData:(NSData* )data;
- (BOOL) isValid;
- (BOOL) isRequestHtml;
- (NSData *) error;
- (NSData *) success;
- (NSData *) success: (NSData *)data;
@end
#endif

